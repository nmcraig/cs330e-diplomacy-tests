#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
    # ----
    # solve
    # ----

    def test_solve_1 (self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_2 (self):
        r = StringIO("A Madrid Hold\n B Barcelona Move Madrid\n C London Move Madrid\n D Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_3 (self):
        r = StringIO("A Madrid Hold\n B Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_4 (self):
        r = StringIO("A Madrid Hold\n B Barcelona Move Madrid\n C London Support B\n D Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")


# ----
# main
# ----


if __name__ == "__main__":
    main()

"""
% coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1
% cat TestDiplomacy.out
% coverage report -m                   >> TestDiplomacy.out
% cat TestDiplomacy.out
"""
